package com.example.optorpm

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import kotlinx.android.synthetic.main.fragment_color_picker.*
import kotlinx.android.synthetic.main.fragment_color_picker.view.*
import java.lang.Integer.min
import kotlin.math.max

class ColorPickerFragment : Fragment() {
    var selectedColor: Int = Color.rgb(127, 127, 127)
        set(value) {
            field = value
            changeColor()
        }
    var minVal: Int = -10
        set(value) {
            field = value
            darkestColor.setBackgroundColor(computeMinMaxColorOwn(value))
            if (minThreshold.progress != -value) minThreshold.progress = -value
        }
    var maxVal: Int = 10
        set(value) {
            field = value
            lightestColor.setBackgroundColor(computeMinMaxColorOwn(value))
            if (maxThreshold.progress != value) maxThreshold.progress = value
        }
    var pickColorCallbackButton: (() -> Int) = { selectedColor }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v =  inflater.inflate(R.layout.fragment_color_picker, container, false)

        v.minThreshold.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean) {
                minVal = -progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        v.maxThreshold.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean) {
                maxVal = progress
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}
            override fun onStopTrackingTouch(seekBar: SeekBar?) {}
        })

        v.colorPickerButton.setOnClickListener {
//            try {
                selectedColor = pickColorCallbackButton()
//            } catch (e: NullPointerException) {
//                Log.d("NoColorFound", e.toString())
//            }
        }

        return v
    }

    private fun changeColor() {
        pickedColor.setBackgroundColor(selectedColor)
        darkestColor.setBackgroundColor(computeMinMaxColorOwn(minVal))
        lightestColor.setBackgroundColor(computeMinMaxColorOwn(maxVal))
    }

    private fun computeMinMaxColorOwn(interval: Int): Int {
        return ColorPickerFragment.computeMinMaxColor(selectedColor, interval)
    }

    companion object {
        @JvmStatic
        fun computeMinMaxColor(selectedColor: Int, interval: Int): Int {
            if (interval == 0) return selectedColor

            var m = max(max(selectedColor.red, selectedColor.green), selectedColor.blue)
            if (m == 0) {
                return Color.rgb(
                    limitColorInterval(selectedColor.red + interval),
                    limitColorInterval(selectedColor.green + interval),
                    limitColorInterval(selectedColor.blue + interval)
                )
            }

            if (interval < 0 && m + interval < 0) {
                return Color.BLACK
            }

            return Color.rgb(
                limitColorInterval(selectedColor.red + (max(0.1f, (selectedColor.red.toFloat() / m.toFloat())) * interval.toFloat()).toInt()),
                limitColorInterval(selectedColor.green + (max(0.1f, (selectedColor.green.toFloat() / m.toFloat())) * interval.toFloat()).toInt()),
                limitColorInterval(selectedColor.blue + (max(0.1f, (selectedColor.blue.toFloat() / m.toFloat())) * interval.toFloat()).toInt())
            )
        }

        @JvmStatic
        fun limitColorInterval(comp: Int): Int {
            return max(0, min(255, comp))
        }
    }
}