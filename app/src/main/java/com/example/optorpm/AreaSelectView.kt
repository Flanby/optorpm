package com.example.optorpm

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class AreaSelectView : View {
    private var paint: Paint? = null
    var x: Float? = null
    var y: Float? = null

    constructor(context: Context) : super(context) {
        init(null, 0)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(attrs, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        init(attrs, defStyle)
    }

    private fun init(attrs: AttributeSet?, defStyle: Int) {
        // Load attributes
        /*
        val a = context.obtainStyledAttributes(
            attrs, R.styleable.AreaSelectView, defStyle, 0
        )

        _exampleString = a.getString(
            R.styleable.AreaSelectView_exampleString
        )
        if (a.hasValue(R.styleable.AreaSelectView_exampleDrawable)) {
            exampleDrawable = a.getDrawable(
                R.styleable.AreaSelectView_exampleDrawable
            )
            exampleDrawable?.callback = this
        }

        a.recycle()
        */

        // Set up a default TextPaint object
        paint = Paint().apply {
            flags = Paint.ANTI_ALIAS_FLAG
            style = Paint.Style.STROKE
            strokeWidth = 5F
            color = Color.GREEN
        }

    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        try {
            canvas.drawCircle(x!!, y!!, 50F, this.paint!!)
        } catch (e: NullPointerException) {
            x = width / 2F
            y = height / 2F

            canvas.drawCircle(x!!, y!!, 50F, this.paint!!)
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        super.onTouchEvent(event)
        var update = false;

        if (0F <= event?.x!! && event?.x!! <= width.toFloat()) {
            x = event?.x
            update = true
        }
        if (0F <= event?.y!! && event?.y!! <= width.toFloat()) {
            y = event?.y
            update = true
        }
        if (update) {
            invalidate()
        }

        return true
    }
}
