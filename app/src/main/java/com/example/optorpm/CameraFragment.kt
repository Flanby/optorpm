package com.example.optorpm

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.*
import android.hardware.camera2.*
import android.hardware.camera2.params.StreamConfigurationMap
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.util.Log
import android.util.Range
import android.util.Size
import android.view.*
import android.view.TextureView.SurfaceTextureListener
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.graphics.blue
import androidx.core.graphics.get
import androidx.core.graphics.green
import androidx.core.graphics.red
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_camera.*
import kotlin.math.max
import kotlinx.android.synthetic.main.activity_main.*


class CameraFragment : Fragment() {
    private var mBackgroundHandler: Handler? = null
    private var mBackgroundThread: HandlerThread? = null

    private var mCamera: CameraDevice? = null
    private var mFPSRange: Range<Int> = Range(120, 120)
    private var mVideoSize: Size = Size(1280,720)

    private var cameraCaptureSessions: CameraCaptureSession? = null
    private var cameraCaptureSessionsHighSpeed: CameraConstrainedHighSpeedCaptureSession?  = null
    private var captureRequestBuilder: CaptureRequest.Builder? = null

    protected var t: Long = 0
    protected var tRPM: Long = 0

    private var isLastDark: Boolean = false

    var updateCallback: ((Long, Bitmap, Int, Int) -> Unit)? = null

    private val stateCallback: CameraDevice.StateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(camera: CameraDevice) {
            Log.d("LoL", "onOpened")
            mCamera = camera
            createCameraPreview()
        }
        override fun onDisconnected(camera: CameraDevice) { mCamera?.close() }
        override fun onError(camera: CameraDevice, error: Int) {
            mCamera?.close()
            mCamera = null
        }
    }

    private var textureListener: TextureView.SurfaceTextureListener = object :
        TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(
            surface: SurfaceTexture,
            width: Int,
            height: Int
        ) {
            //open your camera here
            openCamera(width, height)
        }

        override fun onSurfaceTextureSizeChanged(
            surface: SurfaceTexture,
            width: Int,
            height: Int
        ) {
            // Transform you image captured size according to the surface width and height
        }

        override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
            return false
        }

        override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
            var t2 = System.nanoTime()
            var interval = t2 - t
            // Log.d("FPS", (1000000000 / (t2 - t)).toString())
            t = t2

            try {
                val bitmap = cameraPreview!!.getBitmap(cameraPreview.width / 4, cameraPreview.height / 4)!!

                //Log.d("SIZE", "x: " + cameraPreview.width.toString() + " / y: " + cameraPreview.height.toString())
                // Log.d("SIZE", "x: " + bitmap.width.toString() + " / y: " + bitmap.height.toString())

                var cx = areaSelect.x!!.toInt() / 4
                var cy = areaSelect.y!!.toInt() / 4

                updateCallback!!(interval, bitmap, cx, cy)
            } catch (e: NullPointerException) {
                Log.d("CheckErr", e.toString())
                Toast.makeText(activity!!.applicationContext, "Artung", Toast.LENGTH_SHORT).show()
            }

/*
            try {
                val s: Surface = Surface(cameraPreview.surfaceTexture)
                val canvas: Canvas = s.lockCanvas(Rect(0,0,720,720))!!

                val paint = Paint()
                paint.color = Color.RED
                paint.style = Paint.Style.STROKE

                canvas.drawCircle((canvas.width / 2).toFloat(), (canvas.height / 2).toFloat(), 10F, paint)

                s.unlockCanvasAndPost(canvas)
            } catch (e: NullPointerException) {
                Toast.makeText(activity!!.applicationContext, "Artung", Toast.LENGTH_SHORT).show()
            }*/

        }
    }

    /** Check if this device has a camera */
    private fun checkCameraHardware(context: Context): Boolean {
        return context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)
    }

    fun getColor(): Int {
        val bitmap = cameraPreview!!.getBitmap(cameraPreview.width / 4, cameraPreview.height / 4)!!

        var cx = areaSelect.x!!.toInt() / 4
        var cy = areaSelect.y!!.toInt() / 4

        var avrR: Int = 0
        var avrG: Int = 0
        var avrB: Int = 0
        var avr: Int = 0

        for (ix in -1..1) {
            for (iy in -1..1) {
                val x = cx + ix * 2
                val y = cy + iy * 2

                if (x !in 0 until bitmap.width || y !in 0 until bitmap.height) continue

                val c = bitmap[x, y]

                avrR += c.red
                avrG += c.green
                avrB += c.blue

                avr++
            }
        }

        avrR /= avr
        avrG /= avr
        avrB /= avr

        return Color.rgb(avrR, avrG, avrB)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_camera, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            CameraFragment().apply {
                arguments = Bundle().apply {}
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //cameraPreview = findViewById(R.id.cameraPreview)!!

        if (activity?.applicationContext?.let { this.checkCameraHardware(it) }!!) {
            Toast.makeText(activity!!.applicationContext, "Okay", Toast.LENGTH_SHORT).show()
            //openCamera()
        } else {
            Toast.makeText(activity!!.applicationContext, "Not", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onResume() {
        super.onResume()
        Log.d("LoL", "onResume")
        startBackgroundThread()
        if (cameraPreview!!.isAvailable) {
            openCamera(cameraPreview!!.width, cameraPreview!!.height)
        } else {
            cameraPreview!!.surfaceTextureListener = textureListener
        }
    }

    override fun onPause() {
        Log.e("LoL", "onPause")
        //closeCamera();
        stopBackgroundThread()
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mCamera?.close()
    }


    protected fun startBackgroundThread() {
        mBackgroundThread = HandlerThread("Camera Background")
        mBackgroundThread!!.start()
        mBackgroundHandler = Handler(mBackgroundThread!!.looper)
    }

    protected fun stopBackgroundThread() {
        mBackgroundThread!!.quitSafely()
        try {
            mBackgroundThread!!.join()
            mBackgroundThread = null
            mBackgroundHandler = null
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }

    private fun getHighestFpsRange(fpsRanges: Array<Range<Int>>): Range<Int> {
        val fpsRange =
            Range.create(fpsRanges[0].lower, fpsRanges[0].upper)
        for (r in fpsRanges) {
            if (r.upper > fpsRange.upper) {
                fpsRange.extend(0, r.upper)
            }
        }
        for (r in fpsRanges) {
            if (r.upper == fpsRange.upper) {
                if (r.lower < fpsRange.lower) {
                    fpsRange.extend(r.lower, fpsRange.upper)
                }
            }
        }
        return fpsRange
    }


    private fun openCamera(width: Int, height: Int) {
        var manager: CameraManager = activity?.getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            //manager.cameraIdList.forEach { Log.d("LoL", it) }

            val cameraId: String = manager.cameraIdList[0]
            val characteristics: CameraCharacteristics = manager.getCameraCharacteristics(cameraId)
            val map: StreamConfigurationMap = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)!!

            map.highSpeedVideoSizes
            map.getHighSpeedVideoFpsRangesFor(mVideoSize)

            mFPSRange = getHighestFpsRange(map.highSpeedVideoFpsRanges)
            Log.d("LoL", mFPSRange.toString())

            //imageDimension = map!!.getOutputSizes(SurfaceTexture::class.java)[0]
            configureTransform()

            // Add permission for camera and let user grant the permission
            if (activity?.applicationContext?.let { ActivityCompat.checkSelfPermission(it, Manifest.permission.CAMERA) } != PackageManager.PERMISSION_GRANTED) {
                activity?.let { ActivityCompat.requestPermissions(it, arrayOf(Manifest.permission.CAMERA), 200) }
                return
            }
            manager.openCamera(cameraId, stateCallback, null)
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    protected fun updatePreview() {
        if (null == mCamera) {
            Log.e("LoL", "updatePreview error, return")
        }
        // captureRequestBuilder!!.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_USE_SCENE_MODE)
        // captureRequestBuilder!!.set(CaptureRequest.CONTROL_SCENE_MODE, CameraMetadata.CONTROL_SCENE_MODE_LANDSCAPE)
        captureRequestBuilder!!.set( CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, mFPSRange)
        try {
            /*
            cameraCaptureSessions!!.setRepeatingRequest(
                captureRequestBuilder!!.build(),
                null,
                mBackgroundHandler
            )
            */
            cameraCaptureSessionsHighSpeed!!.setRepeatingBurst(
                cameraCaptureSessionsHighSpeed!!.createHighSpeedRequestList(captureRequestBuilder!!.build()),
                null,
                mBackgroundHandler
            )

        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun configureTransform() {
        val activity: Activity? = activity
        if (null == cameraPreview || null == mVideoSize || null == activity) {
            return
        }
        val rotation = activity.windowManager.defaultDisplay.rotation
        val matrix = Matrix()
        val viewRect = RectF(0F, 0F, cameraPreview.width.toFloat(), cameraPreview.height.toFloat())
        val bufferRect = RectF(0F, 0F, mVideoSize.height.toFloat(), mVideoSize.width.toFloat())
        val centerX = viewRect.centerX()
        val centerY = viewRect.centerY()
        if (Surface.ROTATION_90 === rotation || Surface.ROTATION_270 === rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY())
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL)
            val scale: Float = max(
                cameraPreview.height.toFloat() / mVideoSize.height,
                cameraPreview.width.toFloat() / mVideoSize.width
            )
            matrix.postScale(scale, scale, centerX, centerY)
            matrix.postRotate((90 * (rotation - 2)).toFloat(), centerX, centerY)
        }
        cameraPreview.setTransform(matrix)
    }


    protected fun createCameraPreview() {
        try {
            val texture: SurfaceTexture = cameraPreview?.surfaceTexture!!
            //texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight())

            texture.setDefaultBufferSize(mVideoSize.width, mVideoSize.height)

            val surface = Surface(texture)
            captureRequestBuilder = mCamera?.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW)
            captureRequestBuilder?.addTarget(surface)
            //mCamera?.createCaptureSession(
            mCamera?.createConstrainedHighSpeedCaptureSession(
                MutableList(1) {surface},
                object : CameraCaptureSession.StateCallback() {
                    override fun onConfigured(cameraCaptureSession: CameraCaptureSession) {
                        //The camera is already closed
                        if (null == mCamera) {
                            return
                        }
                        // When the session is ready, we start displaying the preview.
                        cameraCaptureSessions = cameraCaptureSession
                        cameraCaptureSessionsHighSpeed =
                            cameraCaptureSessions as CameraConstrainedHighSpeedCaptureSession
                        updatePreview()
                    }

                    override fun onConfigureFailed(cameraCaptureSession: CameraCaptureSession) {
                        Toast.makeText(activity, "Configuration change", Toast.LENGTH_SHORT).show()
                    }
                },
                null
            )
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }
}