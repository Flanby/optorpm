package com.example.optorpm

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_r_p_m.*
import kotlin.math.max
import kotlin.math.min

const val MIN_FRAME_PER_TURN: Int = 3

class RPMFragment : Fragment() {
    var videoFPS: Long = 1
        set(value) {
            field = 1000000000 / value
            handleColorAndDisplay()
        }

    var currentRPM: Long = 0
        set(value) {
            field = (1000000000.0 / value.toDouble() * 60.0).toLong()
            handleColorAndDisplay()
        }

    private fun handleColorAndDisplay() {
        if (currentRPM.equals(0)) {
            val color: Int = Color.rgb(0,255, 0)

            RPMTextFront.text = currentRPM.toString()
            RPMTextFront.setTextColor(color)
            RPMUnit.setTextColor(color)

            return
        }

        val percent: Double = videoFPS.toDouble() / MIN_FRAME_PER_TURN.toDouble() * 60.0 / currentRPM.toDouble()
        val color: Int = Color.rgb(max(0, (255.0 - 255.0 * max(0.0, percent - 0.5)).toInt()), min((percent * 2.0 * 255).toInt(), 255), 0)

        RPMTextFront.text = currentRPM.toString()
        RPMTextFront.setTextColor(color)
        RPMUnit.setTextColor(color)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_r_p_m, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            RPMFragment().apply {
                arguments = Bundle().apply {}
            }
    }
}