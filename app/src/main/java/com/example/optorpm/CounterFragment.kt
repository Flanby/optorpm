package com.example.optorpm

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_counter.*
import kotlin.math.pow

class CounterFragment : Fragment() {
    var digits: Int = 5
        set(value) {
            field = value
            count = count
            BackgroundText.text = "8".repeat(field)
        }

    var count: Int = 0
        set(value) {
            field = value % 10.0.pow(digits).toInt()
            CounterText.text = field.toString()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            digits = it.getInt("Digits")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_counter, container, false)
    }

    companion object {
        @JvmStatic
        fun newInstance(d: Int) =
            CounterFragment().apply {
                arguments = Bundle().apply {
                    putInt("Digits", d)
                }
            }
    }

    fun incr() {
        count += 1
    }

    fun reset() {
        count = 0
    }
}