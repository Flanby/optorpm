package com.example.optorpm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView

class SplashActivity : AppCompatActivity() {
    private val SPLASH_TIME_OUT:Long = 1000
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        //val rays: ImageView = findViewById(R.id.rays)
        //rays.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.rotate_indefinitely))

        Handler().postDelayed({

            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, SPLASH_TIME_OUT)
    }
}