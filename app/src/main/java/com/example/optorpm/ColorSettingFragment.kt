package com.example.optorpm

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import kotlinx.android.synthetic.main.fragment_color_setting.*
import kotlinx.android.synthetic.main.fragment_color_setting.view.*

private const val ARG_COLOR1 = "color1"
private const val ARG_COLOR2 = "color2"

class ColorSettingFragment : Fragment() {
    var closeCallback: (() -> Unit)? = null

    var c1Val: Int = 0
    var c1Min: Int = -10
    var c1Max: Int = 10
    var c2Val: Int = Color.rgb(245, 245, 245)
    var c2Min: Int = -10
    var c2Max: Int = 10

    var c1MinVal: Int = 0
    var c1MaxVal: Int = 0
    var c2MinVal: Int = 0
    var c2MaxVal: Int = 0

    var pickColorCallback: (() -> Int) = { 0 }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //arguments?.let {
        //    c1Val = it.getInt(ARG_COLOR1)
        //    c2Val = it.getInt(ARG_COLOR2)
        //}
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_color_setting, container, false)

        (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).selectedColor = c1Val
        (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).minVal = c1Min
        (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).maxVal = c1Max
        (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).selectedColor = c2Val
        (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).minVal = c2Min
        (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).maxVal = c2Max

        v.close.setOnClickListener {
            c1Val = (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).selectedColor
            c1Min = (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).minVal
            c1Max = (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).maxVal
            c2Val = (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).selectedColor
            c2Min = (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).minVal
            c2Max = (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).maxVal

            c1MinVal = ColorPickerFragment.computeMinMaxColor(c1Val, c1Min)
            c1MaxVal = ColorPickerFragment.computeMinMaxColor(c1Val, c1Max)
            c2MinVal = ColorPickerFragment.computeMinMaxColor(c2Val, c2Min)
            c2MaxVal = ColorPickerFragment.computeMinMaxColor(c2Val, c2Max)

            if (closeCallback != null) {
                closeCallback!!()
            }
        }

        (childFragmentManager.findFragmentById(R.id.color1) as ColorPickerFragment).pickColorCallbackButton = {
            pickColorCallback()
        }

        (childFragmentManager.findFragmentById(R.id.color2) as ColorPickerFragment).pickColorCallbackButton = {
            pickColorCallback()
        }

        return v;
    }

    fun isColorInRangeForDark(color: Int): Boolean {
        return color.red in c1MinVal.red..c1MaxVal.red && color.green in c1MinVal.green..c1MaxVal.green && color.blue in c1MinVal.blue..c1MaxVal.blue
    }

    fun isColorInRangeForLight(color: Int): Boolean {
        return color.red in c2MinVal.red..c2MaxVal.red && color.green in c2MinVal.green..c2MaxVal.green && color.blue in c2MinVal.blue..c2MaxVal.blue
    }

    companion object {
        @JvmStatic
        fun newInstance(color1: Int, color2: Int) =
            ColorSettingFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLOR1, color1)
                    putInt(ARG_COLOR2, color2)
                }
            }
    }
}