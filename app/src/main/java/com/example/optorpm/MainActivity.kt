package com.example.optorpm

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.graphics.blue
import androidx.core.graphics.get
import androidx.core.graphics.green
import androidx.core.graphics.red
import kotlinx.android.synthetic.main.activity_main.*

class CameraCallback {
    companion object {
        var isLastDark: Boolean = false
        var tRPM: Long = System.nanoTime()

        var update: ((Long, Bitmap, Int, Int) -> Unit)? = null
    }
}

class MainActivity : AppCompatActivity() {
    private val settingFragment: ColorSettingFragment = ColorSettingFragment.newInstance(Color.rgb(10, 10, 10), Color.rgb(245, 245, 245))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        (fragmentCamera as CameraFragment).updateCallback = { interval: Long, bitmap: Bitmap, cx: Int, cy: Int ->
            (fragmentRPM as RPMFragment).videoFPS = interval

            var avrR: Int = 0
            var avrG: Int = 0
            var avrB: Int = 0
            var avr: Int = 0

            for (ix in -1..1) {
                for (iy in -1..1) {
                    val x = cx + ix * 2
                    val y = cy + iy * 2

                    if (x !in 0 until bitmap.width || y !in 0 until bitmap.height) continue

                    val c = bitmap[x, y]

                    avrR += c.red
                    avrG += c.green
                    avrB += c.blue

                    avr++
                }
            }

            avrR /= avr
            avrG /= avr
            avrB /= avr


            var color = Color.rgb(avrR, avrG, avrB)

            if ((!CameraCallback.isLastDark && settingFragment.isColorInRangeForDark(color)) || (CameraCallback.isLastDark && settingFragment.isColorInRangeForLight(color))) {
                CameraCallback.isLastDark = !CameraCallback.isLastDark
                Log.d("CheckTrue", "$avrR / $avrG / $avrB")

                if (CameraCallback.isLastDark) {
                    (fragmentCounter as CounterFragment).incr()

                    var t2 = System.nanoTime()
                    (fragmentRPM as RPMFragment).currentRPM = t2 - CameraCallback.tRPM
                    CameraCallback.tRPM = t2
                }

            } else {
                // Log.d("Check", "$avrR / $avrG / $avrB")
            }
        }

        Refresh.setOnClickListener {
            (fragmentCounter as CounterFragment).reset()
        }

        settingButton.setOnClickListener {
            Log.d("Click", "Click Settings")
            supportFragmentManager.beginTransaction().add(R.id.settingFragmentFrame, settingFragment).commit()
        }

        settingFragment.closeCallback = {
            supportFragmentManager.beginTransaction().remove(settingFragment).commit()
        }

        settingFragment.pickColorCallback = {
            (fragmentCamera as CameraFragment).getColor()
        }
    }
}